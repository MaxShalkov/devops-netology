# Домашнее задание к занятию "6.2. SQL"

## Введение

## Задача 1

Используя docker поднимите инстанс PostgreSQL (версию 12) c 2 volume, 
в который будут складываться данные БД и бэкапы.

Приведите получившуюся команду или docker-compose манифест.

```
    version: "3"

    volumes:
      postgres_backup:
        external: true
      postgres_db:
        external: true

    services:
      postgres:
        image: postgres:12-alpine
          volumes:
            - postgres_backup:/etc/postgres_backup
            - postgres_db:/etc/postgres_db
          environment:
            POSTGRES_DB: "postgres"
            POSTGRES_USER: "postgres"
            POSTGRES_PASSWORD: "postgres"
            PGDATA: /etc/postgres_db/pgdata
          ports:
            - "0.0.0.0:5432:5432"
```

## Задача 2

В БД из задачи 1: 
- создайте пользователя test-admin-user и БД test_db
```
CREATE DATABASE test_db;
CREATE USER "test-admin-user" with encrypted password 'mypass';
```

- в БД test_db создайте таблицу orders и clients (спeцификация таблиц ниже)
```
    CREATE TABLE orders ( 
        id serial PRIMARY KEY, 
        name text, 
        price integer
    );
    CREATE TABLE clients (
        id serial PRIMARY KEY, 
        surname text,
        country text,
        zakaz integer,
        FOREIGN KEY (zakaz) REFERENCES orders (id)
    );
    CREATE INDEX ON clients (country);
```

- предоставьте привилегии на все операции пользователю test-admin-user на таблицы БД test_db
```
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO "test-admin-user";
```

- создайте пользователя test-simple-user 
- предоставьте пользователю test-simple-user права на SELECT/INSERT/UPDATE/DELETE данных таблиц БД test_db
```
    CREATE USER "test-simple-user" with encrypted password 'mypass';
    GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO "test-simple-user";
```

Таблица orders:
- id (serial primary key)
- наименование (string)
- цена (integer)

Таблица clients:
- id (serial primary key)
- фамилия (string)
- страна проживания (string, index)
- заказ (foreign key orders)

Приведите:
- итоговый список БД после выполнения пунктов выше,
>Войти в psql: `psql -U postgres`, выбрать базу `\c test_db`

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/6.2.1.png?raw=true)
- описание таблиц (describe)
>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/6.2.2.png?raw=true)
- SQL-запрос для выдачи списка пользователей с правами над таблицами test_db
```
    SELECT grantee, privilege_type 
    FROM information_schema.role_table_grants 
    WHERE table_name='clients';
```
- список пользователей с правами над таблицами test_db
>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/6.2.3.png?raw=true)




## Задача 3

Используя SQL синтаксис - наполните таблицы следующими тестовыми данными:

Таблица orders

|Наименование|цена|
|------------|----|
|Шоколад| 10 |
|Принтер| 3000 |
|Книга| 500 |
|Монитор| 7000|
|Гитара| 4000|

>`INSERT INTO orders VALUES (1, 'Шоколад', 10), (2, 'Принтер', 3000), (3, 'Книга', 500), (4, 'Монитор', 7000), (5, 'Гитара', 4000);`

Таблица clients

|ФИО|Страна проживания|
|------------|----|
|Иванов Иван Иванович| USA |
|Петров Петр Петрович| Canada |
|Иоганн Себастьян Бах| Japan |
|Ронни Джеймс Дио| Russia|
|Ritchie Blackmore| Russia|

>`INSERT INTO clients VALUES (1, 'Иванов Иван Иванович', 'USA'), (2, 'Петров Петр Петрович', 'Canada'), (3, 'Иоганн Себастьян Бах', 'Japan'), (4, 'Ронни Джеймс Дио', 'Russia'), (5, 'Ritchie Blackmore', 'Russia');`

Используя SQL синтаксис:
- вычислите количество записей для каждой таблицы 
- приведите в ответе:
    - запросы 
    - результаты их выполнения.
>`SELECT COUNT (*) FROM orders;`

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/6.2.4.png?raw=true)

## Задача 4

Часть пользователей из таблицы clients решили оформить заказы из таблицы orders.

Используя foreign keys свяжите записи из таблиц, согласно таблице:

|ФИО|Заказ|
|------------|----|
|Иванов Иван Иванович| Книга |
|Петров Петр Петрович| Монитор |
|Иоганн Себастьян Бах| Гитара |

Приведите SQL-запросы для выполнения данных операций.
>`UPDATE clients SET zakaz = 3 WHERE id = 1;UPDATE clients SET zakaz = 4 WHERE id = 2;UPDATE clients SET zakaz = 5 WHERE id = 3;`

Приведите SQL-запрос для выдачи всех пользователей, которые совершили заказ, а также вывод данного запроса.
>`SELECT * FROM clients WHERE zakaz IS NOT NULL;`

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/6.2.5.png?raw=true)

## Задача 5

Получите полную информацию по выполнению запроса выдачи всех пользователей из задачи 4 
(используя директиву EXPLAIN).
>`EXPLAIN SELECT * FROM clients WHERE zakaz IS NOT NULL;`

Приведите получившийся результат и объясните что значат полученные значения.
>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/6.2.6.png?raw=true)

>`EXPLAIN` выводит информацию о плане выполнения конкретного запроса. Чтение данных из таблицы может выполняться несколькими способами. **Seq Scan** — последовательное, блок за блоком, чтение данных таблицы. Параметр **cost(затраты)** — это оценки времени, которое, как ожидается, займет узел. По умолчанию затраты указаны в единицах *времени последовательного чтения блока размером 8 КБ*. Каждый узел имеет две затраты: *стартовую* стоимость и *общую* стоимость. *Rows* - редполагаемое количество строк, *width* предполагаемая ширина каждой строки. Планировщик выбирает план на основе самого дешевого варианта.

## Задача 6

Создайте бэкап БД test_db и поместите его в volume, предназначенный для бэкапов (см. Задачу 1).
>`pg_dump test_db -Upostgres > /etc/postgres_backup/test_db_12062022.dump`

Остановите контейнер с PostgreSQL (но не удаляйте volumes).

Поднимите новый пустой контейнер с PostgreSQL.
>`docker run -e POSTGRES_PASSWORD=postgres -p 5432:5432 -v postgres_backup:/etc/postgres_backup -d postgres:12-alpine`

Восстановите БД test_db в новом контейнере.
>`createdb test_db -Upostgres`
`psql test_db -Upostgres < /etc/postgres_backup/test_db_12062022.dump`

Приведите список операций, который вы применяли для бэкапа данных и восстановления. 