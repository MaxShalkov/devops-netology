docker run -d -it --name ubuntu ubuntu:22.04 && \
docker exec -it ubuntu apt-get update && \
docker exec -it ubuntu apt-get install python3-pip -y

docker run -d -it --name centos7 centos

docker run -d -it --name fedora pycontribs/fedora

ansible-playbook site.yml -i inventory/ --ask-vault-pass

docker stop ubuntu && docker rm ubuntu
docker stop centos7 && docker rm centos7
docker stop fedora && docker rm fedora
