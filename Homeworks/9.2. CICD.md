# Домашнее задание к занятию "09.02 CI\CD"

## Знакомоство с SonarQube

### Подготовка к выполнению

1. Выполняем `docker pull sonarqube:8.7-community`
2. Выполняем `docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:8.7-community`
3. Ждём запуск, смотрим логи через `docker logs -f sonarqube`
4. Проверяем готовность сервиса через [браузер](http://localhost:9000)
5. Заходим под admin\admin, меняем пароль на свой

В целом, в [этой статье](https://docs.sonarqube.org/latest/setup/install-server/) описаны все варианты установки, включая и docker, но так как нам он нужен разово, то достаточно того набора действий, который я указал выше.

### Основная часть

1. Создаём новый проект, название произвольное
2. Скачиваем пакет sonar-scanner, который нам предлагает скачать сам sonarqube
```
Скачать сканнер можно отсюда https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/
Также необходимо выполнить chmod 755 ./sonar-scanner/jre/bin/java
```
3. Делаем так, чтобы binary был доступен через вызов в shell (или меняем переменную PATH или любой другой удобный вам способ)
```
export PATH=$(pwd):$PATH
```
4. Проверяем `sonar-scanner --version`
5. Запускаем анализатор против кода из директории [example](./example) с дополнительным ключом `-Dsonar.coverage.exclusions=fail.py`
6. Смотрим результат в интерфейсе
7. Исправляем ошибки, которые он выявил(включая warnings)
8. Запускаем анализатор повторно - проверяем, что QG пройдены успешно
9. Делаем скриншот успешного прохождения анализа, прикладываем к решению ДЗ

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/9.2.1.png?raw=true)

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/9.2.2.png?raw=true)

## Знакомство с Nexus

### Подготовка к выполнению

1. Выполняем `docker pull sonatype/nexus3`
2. Выполняем `docker run -d -p 8081:8081 --name nexus sonatype/nexus3`
3. Ждём запуск, смотрим логи через `docker logs -f nexus`
4. Проверяем готовность сервиса через [бразуер](http://localhost:8081)
5. Узнаём пароль от admin через `docker exec -it nexus /bin/bash`
6. Подключаемся под админом, меняем пароль, сохраняем анонимный доступ

### Основная часть

1. В репозиторий `maven-public` загружаем артефакт с GAV параметрами:
   1. groupId: netology
   2. artifactId: java
   3. version: 8_282
   4. classifier: distrib
   5. type: tar.gz
2. В него же загружаем такой же артефакт, но с version: 8_102
3. Проверяем, что все файлы загрузились успешно
4. В ответе присылаем файл `maven-metadata.xml` для этого артефекта

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/9.2.3.png?raw=true)

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/9.2.5.png?raw=true)


### Знакомство с Maven

### Подготовка к выполнению

1. Скачиваем дистрибутив с [maven](https://maven.apache.org/download.cgi)
2. Разархивируем, делаем так, чтобы binary был доступен через вызов в shell (или меняем переменную PATH или любой другой удобный вам способ)
3. Проверяем `mvn --version`
4. Забираем директорию [mvn](./mvn) с pom

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/9.2.4.png?raw=true)

### Основная часть

1. Меняем в `pom.xml` блок с зависимостями под наш артефакт из первого пункта задания для Nexus (java с версией 8_282)
2. Запускаем команду `mvn package` в директории с `pom.xml`, ожидаем успешного окончания

```cmd
   root@netology:/home/ntsadmin/9.2# mvn package
   WARNING: An illegal reflective access operation has occurred
   WARNING: Illegal reflective access by com.google.inject.internal.cglib.core.$ReflectUtils$1 (file:/usr/share/maven/lib/guice.jar) to method java.lang.ClassLoader.defineClass(java.lang.String,byte[],int,int,java.security.ProtectionDomain)
   WARNING: Please consider reporting this to the maintainers of com.google.inject.internal.cglib.core.$ReflectUtils$1
   WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
   WARNING: All illegal access operations will be denied in a future release
   [INFO] Scanning for projects...
   [INFO]
   [INFO] --------------------< com.netology.app:simple-app >---------------------
   [INFO] Building simple-app 1.0-SNAPSHOT
   [INFO] --------------------------------[ jar ]---------------------------------
   Downloading from my-repo: http://shalkov-test:8081/repository/maven-public/netology/java/8_282/java-8_282.pom
   Downloading from central: https://repo.maven.apache.org/maven2/netology/java/8_282/java-8_282.pom
   [WARNING] The POM for netology:java:tar.gz:distrib:8_282 is missing, no dependency information available
   Downloading from my-repo: http://shalkov-test:8081/repository/maven-public/netology/java/8_282/java-8_282-distrib.tar.gz
   Downloaded from my-repo: http://shalkov-test:8081/repository/maven-public/netology/java/8_282/java-8_282-distrib.tar.gz (0 B at 0 B/s)
   [INFO]
   [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ simple-app ---
   [WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
   [INFO] skip non existing resourceDirectory /home/ntsadmin/9.2/src/main/resources
   [INFO]
   [INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ simple-app ---
   [INFO] No sources to compile
   [INFO]
   [INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ simple-app ---
   [WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
   [INFO] skip non existing resourceDirectory /home/ntsadmin/9.2/src/test/resources
   [INFO]
   [INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ simple-app ---
   [INFO] No sources to compile
   [INFO]
   [INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ simple-app ---
   [INFO] No tests to run.
   [INFO]
   [INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ simple-app ---
   [WARNING] JAR will be empty - no content was marked for inclusion!
   [INFO] Building jar: /home/ntsadmin/9.2/target/simple-app-1.0-SNAPSHOT.jar
   [INFO] ------------------------------------------------------------------------
   [INFO] BUILD SUCCESS
   [INFO] ------------------------------------------------------------------------
   [INFO] Total time:  2.047 s
   [INFO] Finished at: 2022-08-09T21:40:49+03:00
   [INFO] ------------------------------------------------------------------------
```

3. Проверяем директорию `~/.m2/repository/`, находим наш артефакт

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/9.2.6.png?raw=true)

4. В ответе присылаем исправленный файл `pom.xml`

>[POM](./9.2/pom.xml)