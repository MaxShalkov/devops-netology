# Создание SA и назначение ролей в YC через terraform. При уничтожении сохраняется ошибка

```terraform
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  cloud_id  =  "b1gft7onlquqaa905o3i"
  folder_id =  "b1gsv3rtu1q428jd7g23"
  zone      =  "ru-central1-a"
}

resource "yandex_iam_service_account" "sa" {
  name = "vmmanager"
}

resource "yandex_resourcemanager_folder_iam_binding" "sa_bind" {
  role         = "compute.admin"
  folder_id    = "b1gsv3rtu1q428jd7g23"
  members      = [
    "serviceAccount:${yandex_iam_service_account.sa.id}",
  ]
}

resource "yandex_iam_service_account_iam_member" "admin-account-iam" {
  service_account_id = "${yandex_iam_service_account.sa.id}"
  role               = "storage.admin"
  member             = "serviceAccount:${yandex_iam_service_account.sa.id}"
}
```