# Домашнее задание к занятию "08.02 Работа с Playbook"

## Подготовка к выполнению
1. Создайте свой собственный (или используйте старый) публичный репозиторий на github с произвольным именем.
2. Скачайте [playbook](./playbook/) из репозитория с домашним заданием и перенесите его в свой репозиторий.
3. Подготовьте хосты в соотвeтствии с группами из предподготовленного playbook. 
4. Скачайте дистрибутив [java](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) и положите его в директорию `playbook/files/`. 

## Основная часть
1. Приготовьте свой собственный inventory файл `prod.yml`.
```yml
---
elasticsearch:
  hosts:
    10.8.91.33:
      ansible_connection: ssh
      ansible_user: ntsadmin

kibana:
  hosts:
    10.8.91.34:
      ansible_connection: ssh
      ansible_user: ntsadmin
```
2. Допишите playbook: нужно сделать ещё один play, который устанавливает и настраивает kibana.
```yml
- name: Install kibana
  hosts: kibana
  tasks:
    - name: Upload tar.gz kibana from remote URL
      get_url:
        url: "{{ kibana_url }}"
        dest: "/tmp/kibana-{{ kibana_version }}-linux-x86_64.tar.gz"
        mode: 0755
        timeout: 60
        force: true
        validate_certs: false
      register: get_kibana
      until: get_kibana is succeeded
      tags: kibana

    - name: Create directrory for Kibana
      become: true
      file:
        state: directory
        path: "{{ kibana_home }}"
      tags: kibana

    - name: Extract kibana in the installation directory
      become: true
      unarchive:
        copy: false
        src: "/tmp/kibana-{{ kibana_version }}-linux-x86_64.tar.gz"
        dest: "{{ kibana_home }}"
        extra_opts: [--strip-components=1]
        creates: "{{ kibana_home }}/bin/kibana"
      tags:
        - kibana

    - name: Set environment kibana
      become: true
      template:
        src: templates/kibana.sh.j2
        dest: /etc/profile.d/kibana.sh
      tags: kibana
```
3. При создании tasks рекомендую использовать модули: `get_url`, `template`, `unarchive`, `file`.
4. Tasks должны: скачать нужной версии дистрибутив, выполнить распаковку в выбранную директорию, сгенерировать конфигурацию с параметрами.
5. Запустите `ansible-lint site.yml` и исправьте ошибки, если они есть.

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.2.4.png?raw=true)

```
Так как elk напрямую недоступен, указаны ссылки для скачивания с ЯндексДиск и они получаются длинными. Решил вынести их в переменные.

ntsadmin@ansible01:~/playbook$ ansible-lint site.yml -vvv
Examining site.yml of type playbook
```
6. Попробуйте запустить playbook на этом окружении с флагом `--check`.
```cmd
ntsadmin@ansible01:~/playbook$ ansible-playbook -i inventory site.yml --check

PLAY [Install Java] ***********************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************
ok: [10.8.91.34]
ok: [10.8.91.33]

TASK [Set facts for Java 11 vars] *********************************************************************************************************
ok: [10.8.91.33]
ok: [10.8.91.34]

TASK [Upload .tar.gz file containing binaries from local storage] *************************************************************************
changed: [10.8.91.34]
changed: [10.8.91.33]

TASK [Ensure installation dir exists] *****************************************************************************************************
changed: [10.8.91.34]
changed: [10.8.91.33]

TASK [Extract java in the installation directory] *****************************************************************************************
fatal: [10.8.91.34]: FAILED! => {"changed": false, "msg": "dest '/opt/jdk/18.0.2' must be an existing dir"}
fatal: [10.8.91.33]: FAILED! => {"changed": false, "msg": "dest '/opt/jdk/18.0.2' must be an existing dir"}

PLAY RECAP ********************************************************************************************************************************
10.8.91.33                 : ok=4    changed=2    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0   
10.8.91.34                 : ok=4    changed=2    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0 
```
7. Запустите playbook на `prod.yml` окружении с флагом `--diff`. Убедитесь, что изменения на системе произведены.
```cmd
ntsadmin@ansible01:~/playbook$ ansible-playbook -i inventory site.yml --diff

PLAY [Install Java] ***********************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************
ok: [10.8.91.34]
ok: [10.8.91.33]

TASK [Set facts for Java 11 vars] *********************************************************************************************************
ok: [10.8.91.33]
ok: [10.8.91.34]

TASK [Upload .tar.gz file containing binaries from local storage] *************************************************************************
diff skipped: source file size is greater than 104448
changed: [10.8.91.34]
diff skipped: source file size is greater than 104448
changed: [10.8.91.33]

TASK [Ensure installation dir exists] *****************************************************************************************************
--- before
+++ after
@@ -1,4 +1,4 @@
 {
     "path": "/opt/jdk/18.0.2",
-    "state": "absent"
+    "state": "directory"
 }

changed: [10.8.91.34]
--- before
+++ after
@@ -1,4 +1,4 @@
 {
     "path": "/opt/jdk/18.0.2",
-    "state": "absent"
+    "state": "directory"
 }

changed: [10.8.91.33]

TASK [Extract java in the installation directory] *****************************************************************************************
changed: [10.8.91.34]
changed: [10.8.91.33]

TASK [Export environment variables] *******************************************************************************************************
--- before
+++ after: /home/ntsadmin/.ansible/tmp/ansible-local-19845ghhk_qy8/tmpi4_ryued/jdk.sh.j2
@@ -0,0 +1,5 @@
+# Warning: This file is Ansible Managed, manual changes will be overwritten on next playbook run.
+#!/usr/bin/env bash
+
+export JAVA_HOME=/opt/jdk/18.0.2
+export PATH=$PATH:$JAVA_HOME/bin
\ No newline at end of file

changed: [10.8.91.34]
--- before
+++ after: /home/ntsadmin/.ansible/tmp/ansible-local-19845ghhk_qy8/tmp0uuaruu9/jdk.sh.j2
@@ -0,0 +1,5 @@
+# Warning: This file is Ansible Managed, manual changes will be overwritten on next playbook run.
+#!/usr/bin/env bash
+
+export JAVA_HOME=/opt/jdk/18.0.2
+export PATH=$PATH:$JAVA_HOME/bin
\ No newline at end of file

changed: [10.8.91.33]

PLAY [Install Elasticsearch] **************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************
ok: [10.8.91.33]

TASK [Upload tar.gz Elasticsearch from remote URL] ****************************************************************************************
changed: [10.8.91.33]

TASK [Create directrory for Elasticsearch] ************************************************************************************************
--- before
+++ after
@@ -1,4 +1,4 @@
 {
     "path": "/opt/elastic/7.10.1",
-    "state": "absent"
+    "state": "directory"
 }

changed: [10.8.91.33]

TASK [Extract Elasticsearch in the installation directory] ********************************************************************************
changed: [10.8.91.33]

TASK [Set environment Elastic] ************************************************************************************************************
--- before
+++ after: /home/ntsadmin/.ansible/tmp/ansible-local-19845ghhk_qy8/tmprw_5gq9t/elk.sh.j2
@@ -0,0 +1,5 @@
+# Warning: This file is Ansible Managed, manual changes will be overwritten on next playbook run.
+#!/usr/bin/env bash
+
+export ES_HOME=/opt/elastic/7.10.1
+export PATH=$PATH:$ES_HOME/bin
\ No newline at end of file

changed: [10.8.91.33]

PLAY [Install kibana] *********************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************
ok: [10.8.91.34]

TASK [Upload tar.gz kibana from remote URL] ***********************************************************************************************
changed: [10.8.91.34]

TASK [Create directrory for Kibana] *******************************************************************************************************
--- before
+++ after
@@ -1,4 +1,4 @@
 {
     "path": "/opt/kibana/7.10.1",
-    "state": "absent"
+    "state": "directory"
 }

changed: [10.8.91.34]

TASK [Extract kibana in the installation directory] ***************************************************************************************
changed: [10.8.91.34]

TASK [Set environment kibana] *************************************************************************************************************
--- before
+++ after: /home/ntsadmin/.ansible/tmp/ansible-local-19845ghhk_qy8/tmpvroxuh80/kibana.sh.j2
@@ -0,0 +1,5 @@
+# Warning: This file is Ansible Managed, manual changes will be overwritten on next playbook run.
+#!/usr/bin/env bash
+
+export KIBANA_HOME=/opt/kibana/7.10.1
+export PATH=$PATH:$KIBANA_HOME/bin
\ No newline at end of file

changed: [10.8.91.34]

PLAY RECAP ********************************************************************************************************************************
10.8.91.33                 : ok=11   changed=8    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
10.8.91.34                 : ok=11   changed=8    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0 
```
8. Повторно запустите playbook с флагом `--diff` и убедитесь, что playbook идемпотентен.
```cmd
ntsadmin@ansible01:~/playbook$ ansible-playbook -i inventory site.yml --diff

PLAY [Install Java] ***********************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************
ok: [10.8.91.34]
ok: [10.8.91.33]

TASK [Set facts for Java 11 vars] *********************************************************************************************************
ok: [10.8.91.33]
ok: [10.8.91.34]

TASK [Upload .tar.gz file containing binaries from local storage] *************************************************************************
ok: [10.8.91.34]
ok: [10.8.91.33]

TASK [Ensure installation dir exists] *****************************************************************************************************
ok: [10.8.91.34]
ok: [10.8.91.33]

TASK [Extract java in the installation directory] *****************************************************************************************
skipping: [10.8.91.33]
skipping: [10.8.91.34]

TASK [Export environment variables] *******************************************************************************************************
ok: [10.8.91.34]
ok: [10.8.91.33]

PLAY [Install Elasticsearch] **************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************
ok: [10.8.91.33]

TASK [Upload tar.gz Elasticsearch from remote URL] ****************************************************************************************
ok: [10.8.91.33]

TASK [Create directrory for Elasticsearch] ************************************************************************************************
ok: [10.8.91.33]

TASK [Extract Elasticsearch in the installation directory] ********************************************************************************
skipping: [10.8.91.33]

TASK [Set environment Elastic] ************************************************************************************************************
ok: [10.8.91.33]

PLAY [Install kibana] *********************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************
ok: [10.8.91.34]

TASK [Upload tar.gz kibana from remote URL] ***********************************************************************************************
ok: [10.8.91.34]

TASK [Create directrory for Kibana] *******************************************************************************************************
ok: [10.8.91.34]

TASK [Extract kibana in the installation directory] ***************************************************************************************
skipping: [10.8.91.34]

TASK [Set environment kibana] *************************************************************************************************************
ok: [10.8.91.34]

PLAY RECAP ********************************************************************************************************************************
10.8.91.33                 : ok=9    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0   
10.8.91.34                 : ok=9    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0
```
9. Подготовьте README.md файл по своему playbook. В нём должно быть описано: что делает playbook, какие у него есть параметры и теги.
10. Готовый playbook выложите в свой репозиторий, в ответ предоставьте ссылку на него.

>https://gitlab.com/lybomir_dobrynin/devops-netology/-/tree/main/Homeworks/8.2/playbook

## Необязательная часть

1. Приготовьте дополнительный хост для установки logstash.
2. Пропишите данный хост в `prod.yml` в новую группу `logstash`.
3. Дополните playbook ещё одним play, который будет исполнять установку logstash только на выделенный для него хост.
4. Все переменные для нового play определите в отдельный файл `group_vars/logstash/vars.yml`.
5. Logstash конфиг должен конфигурироваться в части ссылки на elasticsearch (можно взять, например его IP из facts или определить через vars).
6. Дополните README.md, протестируйте playbook, выложите новую версию в github. В ответ предоставьте ссылку на репозиторий.