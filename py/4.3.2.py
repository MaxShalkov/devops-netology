#!/usr/bin/env python3
import socket, json, os, yaml, argparse

parser = argparse.ArgumentParser()
parser.add_argument("-type", 
                    default="json",
                    type=str,
                    choices=['json','yaml'],
                    help="JSON or YAML")

args = parser.parse_args()

load_dict = ""
list_path = f'{os.path.dirname(os.path.realpath(__file__))}\list.{args.type}'
services = ['drive.google.com',
            'mail.google.com',
            'google.com']
services_list = []

if os.path.exists(list_path):
    load_dict = {}

    with open(list_path) as _file:
        load_list = json.load(_file) if args.type == 'json' else yaml.safe_load(_file)

    for i in load_list:
        load_dict.update(i)

for s in services:
    h = {}
    h[s] = socket.gethostbyname(s)
    services_list.append(h)

    if load_dict:
        if load_dict[s] != h[s]:
            print(f'[ERROR] {s} IP mismatch: {load_dict[s]} {h[s]}')
        else:
            print(f'{s} {h[s]}')    
    else:
        print(f'{s} {h[s]}')


with open(list_path, 'w') as outfile:
    json.dump(services_list, outfile) if args.type == 'json' else yaml.dump(services_list,outfile)